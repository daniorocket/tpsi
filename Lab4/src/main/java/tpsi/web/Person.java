/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.web;

/**
 *
 * @author Danio
 */
public class Person {
    String firstName;
    String lastName;
    String email;
    Person(String firstName,String lastName,String email)
    {
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
    }
    public void setfirstName(String firstName)
    {
        this.firstName=firstName;
    }
    public void setlastName(String lastName)
    {
        this.lastName=lastName;
    }
    public  void setemail(String email)
    {
        this.email=email;
    }
    public String getfirstName()
    {
        return firstName;
    }
    public String getlastName()
    {
        return lastName;
    }
    public String getemail()
    {
        return email;
    }
}
