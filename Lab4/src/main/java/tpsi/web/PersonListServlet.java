/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Danio
 */
@WebServlet(name = "PersonListServlet", urlPatterns = {"/personList"})
public class PersonListServlet extends HttpServlet {

     protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
         if(session.getAttribute("licznik")==null)
        {
            session.setAttribute("licznik",1);
        }
        else
        {
            session.setAttribute("licznik", (Integer)session.getAttribute("licznik")+1);
        }
         request.getRequestDispatcher("personList.jsp").forward(request, response);
    }
      protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          String imie = request.getParameter("imie");
        String nazwisko = request.getParameter("nazwisko");
        String email = request.getParameter("email");
        Person p=new Person(imie,nazwisko,email);
        HttpSession session = request.getSession();
        if(session.getAttribute("licznik")==null)
        {
            session.setAttribute("licznik",1);
        }
        else
        {
            session.setAttribute("licznik", (Integer)session.getAttribute("licznik")+1);
        }
         if(session.getAttribute("lista")==null)
        {
            List<Person> osoby2 = new ArrayList<>();
             osoby2.add(p);
            session.setAttribute("lista",osoby2);
        }
        else
        {
                  List<Person> osoby3 = (List<Person>) session.getAttribute("lista");
                  osoby3.add(p);
            session.setAttribute("lista", osoby3);
        }
        request.getRequestDispatcher("personList.jsp").forward(request, response);
      }
 }