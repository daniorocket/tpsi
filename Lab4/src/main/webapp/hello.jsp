<%-- 
    Document   : hello
    Created on : 2020-03-29, 12:14:58
    Author     : Danio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java Web - laboratorium 4</title>
    </head>
    <body>
        <h1>Witaj, ${person.firstName} ${person.lastName}!</h1>
       <%-- pierwsza opcja bez jtsl przetwarza tekst html do rzeczywistego html, a druga opcja z jtsl traktuje tekst html jako zwykły tekst. --%>
       <%--  Inaczej mówiąc w pierwszej opcji bez jtsl zostanie wyświetlony różowy tekst, oraz komunikat pochodzący z JS  --%>
        <h1>Cześć, <c:out value="${person.firstName}"/>!</h1>
        <c:forEach items="${dniTygodnia}" var="dzien">
            <p>
                ${dzien}
            </p>
        </c:forEach>
        <a href="mailto:${person.email}">e-mail</a>
    </body>
</html>
