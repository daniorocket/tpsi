/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.labs.lab2;

/**
 *
 * @author Student
 */
public class Product {   
    int id;
    String name;
    double price;
    String category;
    public Product(int id,String name,double price,String category) {
        this.id=id;
        this.name=name;
        this.price=price;
        this.category=category;
    }
     public int getid()
     {
         return id;
     }
     public String getCategory()
     {
         return category;
     }
    @Override    
    public String toString() {
        return "\n["+id + ", " + name + ", " + category+ ", " +price+"]";
    }
    
    
    
}
