/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.labs.lab2;

/**
 *
 * @author Danio
 */
public class Teacher extends Person {

    String courseName;

    public Teacher(String firstName, String lastName, String emailAddress, String courseName) {
        super(firstName, lastName, emailAddress);
        this.courseName = courseName;
    }

    public String getcourseName() {
        return courseName;
    }

    public void setcourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " " + courseName;
    }
}
