/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.labs.lab2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Danio
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        University zut = new University("Zachodniopomorski Uniwersystet Technologiczny w Szczecinie", "zut@zut.edu.pl");
        University wsks = new University("Wyższa Szkoła Kosmetologii Stosowanej w Koluszkach", "wsks@wsks.edu.pl");

        Student s1 = new Student("Jan", "Kowalski", "jkowalski@wi.zut.edu.pl", "32A");
        Person s2 = new Student("Hermenegilda", "Nowak", "henowak@wi.zut.edu.pl", "32A");
        Teacher t1 = new Teacher("Bjarne", "Stroustrup", "bjarne@fake.org", "Introduction to C++");
        Faculty f1 = new Faculty("Wydział Informatyki", "wi@zut.edu.pl");
        EmailRecipient[] spamList = new EmailRecipient[5];
        spamList[0] = zut;
        spamList[1] = wsks;
        spamList[2] = s1;
        spamList[3] = s2;
        spamList[4] = f1;
        /*for (EmailRecipient recipient : spamList) {
            String email = recipient.getEmailAddress();
            System.out.println(email);
        }
        s1.addGrade(2.0);
        s1.addGrade(3.0);
        s1.addGrade(5.0);
        System.out.println(s1.getGrades());
        System.out.println(s1.getGradesAverage());
        zut.addFaculty(f1);
        f1.addTeacher(t1);
        System.out.println(zut);
        */
        Map<String, Teacher> osoby = new HashMap<>();
        osoby.put("Introduction to C++", t1);
        osoby.put("Programowanie obiektowe", new Teacher("Mykhailo", "Fedorov","fedorov@zut.edu.pl","Programowanie obiektowe"));
        osoby.put("Technologie programowania systemów internetowych", new Teacher("Michal", "Kramarczyk","kramarczyk@zut.edu.pl","Technologie programowania systemów internetowych"));
        Teacher nauczycielObiektowki = osoby.get("Programowanie obiektowe");
        System.out.println(nauczycielObiektowki);
        List<Student> grupa32 = new ArrayList<>();
        List<Student> grupa31 = new ArrayList<>();
        Map<Integer, List<Student>> grupy = new HashMap<>();
        grupa32.add(new Student("Jan", "Kowalski","kowalski@zut.edu.pl","grupa32"));
        grupa32.add(new Student("Zenon", "Nowak","nowak@zut.edu.pl","grupa32"));
        grupa31.add(new Student("Albert", "Nowakowski","nowakowski@zut.edu.pl","grupa31"));
        grupy.put(32, grupa32);
        grupy.put(31, grupa31);
        List<Student> grupaa32 = grupy.get(32);
        System.out.println(grupaa32);
  String current = new java.io.File( "." ).getCanonicalPath();
        System.out.println("Current dir:"+current);
        
        List<Product> Produkty=new ArrayList<>(); 
        try(BufferedReader in = new BufferedReader(new FileReader("produkty.txt"))) {
    String s = in.readLine();
    while(s!=null) {
        String[] pola = s.split(";");
        int id = Integer.parseInt(pola[0]);
        double price = Double.parseDouble(pola[3]);
        Produkty.add(new Product(id, pola[1],price, pola[2]));
        s = in.readLine();
    }
} catch (IOException ex) {
    ex.printStackTrace();
}
        System.out.println(Produkty);
        Map<Integer,Product> mapaProduktow = new HashMap<>();
        for(Product produkt : Produkty) {
               System.out.println(produkt);
               mapaProduktow.put(produkt.getid(),produkt);
        }
        
        
    }
}
