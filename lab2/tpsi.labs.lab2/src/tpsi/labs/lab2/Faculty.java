/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsi.labs.lab2;

import java.util.*;

/**
 *
 * @author Danio
 */
public class Faculty implements EmailRecipient {

    private String emailAddress;
    private String facultyName;
    private List<Teacher> teachers = new ArrayList<>();

    public Faculty(String facultyName, String emailAddress) {
        this.facultyName = facultyName;
        this.emailAddress = emailAddress;
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String toString() {
        return facultyName + " " + emailAddress + " " + teachers;
    }
}
