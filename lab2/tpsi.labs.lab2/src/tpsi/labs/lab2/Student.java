package tpsi.labs.lab2;

import java.util.*;

public class Student extends Person {

    private String groupId;
    private List<Double> grades = new ArrayList<>();

    public Student(String firstName, String lastName, String emailAddress, String groupId) {
        super(firstName, lastName, emailAddress);
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void addGrade(double grade) {
        grades.add(grade);
    }

    public Double getGradesAverage() {
        Double Sum = 0.0;
        Integer Counter = 0;
        for (Double grade : grades) {
            Sum += grade;
            Counter = Counter + 1;
        }
        return (Double) (Sum / Counter);
    }
}
